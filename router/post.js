const express = require("express");

const { createPost, listPost } = require("../controller/post");

const router = express.Router();

router.post("/create_post", createPost);
router.get("/list_post", listPost);

module.exports = router;
