const express = require("express");

const {
  friendSubscription,
  getFriendsByType,
} = require("../controller/friendSubscription");

const router = express.Router();

router.post("/", getFriendsByType);
router.post("/friend_subscription", friendSubscription);

module.exports = router;
