const { MongoClient } = require('mongodb');

let dbConnection;

const connectToDB = (cb) => {
    let uri = 'mongodb://localhost:27017/notebook_demo';

     MongoClient.connect(uri)
        .then(client => {
            dbConnection = client.db();
            return cb()
        })
        .catch(e => {
            console.log(e)
            return cb(e)
        })
}

const getDB = () => dbConnection

module.exports = {
    connectToDB,
    getDB
}