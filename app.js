const express = require("express");
const dotenv = require("dotenv");
const { connectToDB, getDB } = require("./db");
const auth = require("./middleware/auth");

let db;

// Route list
const userRoutes = require("./router/user");
const friendSubscriptionRoutes = require("./router/friendSubscription");
const postRoutes = require("./router/post");

const app = express();

// Set up Global configuration access
dotenv.config();

app.use(express.json());

// DB connection
connectToDB((err) => {
  if (!err) {
    app.listen(3000, () => {
      console.log("Listening");
    });
    db = getDB();
  }
});

// Route path
app.use("/users", userRoutes);
app.use("/friends", auth, friendSubscriptionRoutes);
app.use("/posts", auth, postRoutes);
