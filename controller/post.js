const { ObjectId } = require("mongodb");
const { getDB } = require("../db");

// to create post
const createPost = async (req, res) => {
  try {
    const { post_title } = req.body;

    // Validate user input
    if (!post_title) {
      return res.status(400).send("post_title field is required");
    }

    // get DB
    const db = getDB();

    // Create user
    await db
      .collection("posts")
      .insertOne({
        post_title,
        user_id: req.user.user_id,
        createtime: new Date().toISOString(),
      })
      .then((result) => {
        console.log(result);
        if (result?.insertedId) {
          // return new post
          return res.status(201).json({
            ...result,
            msg: "Post successfully created",
          });
        }
        return res.status(201).json({ message: "insertedId is missing" });
      })
      .catch((e) => res.status(500).json({ e: JSON.stringify(e) }));
  } catch (e) {
    console.log(e);
    res.status(400).send(JSON.stringify(e));
  }
};

// get all friends by id
const getMyFriendsId = (userId) => {
  let query = [
    {
      $match: { requestorId: ObjectId(userId), status: "ACCECPT" },
    },
    {
      $project: {
        user_id: "$accecptorId",
        _id: 0,
      },
    },
    {
      $unionWith: {
        coll: "user_subscriptions",
        pipeline: [
          {
            $match: { accecptorId: ObjectId(userId), status: "ACCECPT" },
          },
          {
            $project: {
              user_id: "$requestorId",
              _id: 0,
            },
          },
        ],
      },
    },
  ];
  return query;
};

// list all post by user and his friends id
const listPost = async (req, res) => {
  try {
    // get DB
    const db = getDB();

    let query = getMyFriendsId(req.user.user_id);

    // user subscriptions
    await db
      .collection("user_subscriptions")
      .aggregate(query)
      .toArray()
      .then((friendList) => {
        let ids = [req.user.user_id];

        if (friendList?.length) {
          ids = [
            ...ids,
            ...friendList?.map((user) => ObjectId(user?.user_id).toString()),
          ];
        }

        db.collection("posts")
          .find({
            user_id: {
              $in: ids,
            },
          })
          .sort({ createtime: -1 })
          .toArray()
          .then((list) => {
            return res.status(201).json({ list });
          })
          .catch((e) => res.status(500).json({ e: JSON.stringify(e) }));
      })
      .catch((e) => res.status(500).json({ e: JSON.stringify(e) }));
  } catch (e) {
    console.log(e);
    res.status(400).send(JSON.stringify(e));
  }
};

module.exports = {
  createPost,
  listPost,
};
