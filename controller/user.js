const bcrypt = require("bcryptjs");
const { getDB } = require("../db");
const jwt = require("jsonwebtoken");


// to register
const createUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      return res.status(400).send("All input is required");
    }

    // get DB
    const db = getDB();

    // check if user already exist
    const isUserExist = await db.collection("user_profiles").findOne({ email });
    if (isUserExist?._id) {
      return res.status(409).send("User Already Exist");
    }

    //Encrypt user password
    let encryptedPassword = await bcrypt.hash(password.toString(), 10);

    // Create user
    await db
      .collection("user_profiles")
      .insertOne({
        email,
        password: encryptedPassword,
      })
      .then((result) => {
        if (result?.insertedId) {
          // Create token
          const token = jwt.sign(
            { user_id: result?.insertedId, email },
            process.env.TOKEN_KEY,
            { expiresIn: "2h" }
          );

          // return new user
          return res.status(201).json({
            ...result,
            token,
            msg: "Successfully registered",
          });
        }
        return res.status(201).json({ message: "insertedId is missing" });
      })
      .catch((e) => res.status(500).json({ e: JSON.stringify(e) }));
  } catch (e) {
    console.log(e);
    res.status(400).send(JSON.stringify(e));
  }
};

// to register
const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    // Validate user input
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }

    // get DB
    const db = getDB();

    // check if user already exist
    const user = await db.collection("user_profiles").findOne({ email });

    if (!!!user?._id) {
      return res.status(400).send("User doesn't exist");
    }

    const validatePassword = await bcrypt.compare(password, user?.password);

    if (user?._id && validatePassword) {
      // Create token
      const token = jwt.sign(
        { user_id: user?._id, email },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      return res.status(201).json({
        ...user,
        token,
      });
    } else {
      return res.status(400).send("Username and password doesn't match");
    }
  } catch (e) {
    console.log(e);
    res.status(400).send(JSON.stringify(e));
  }
};

module.exports = {
  createUser,
  login,
};
