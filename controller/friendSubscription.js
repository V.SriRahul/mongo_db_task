const { ObjectId } = require("mongodb");
const { getDB } = require("../db");

const STATUS_ENUM = {
  REQUEST: "REQUEST",
  ACCECPT: "ACCECPT",
  REJECT: "REJECT",
};

const FRIEND_TYPE_ENUM = {
  ALL: "ALL",
  REQUESTING: "REQUESTING",
  FOLLOWING: "FOLLOWING",
};

// to request, accept, reject friends based on its status STATUS_ENUM
const friendSubscription = async (req, res) => {
  try {
    const { friendId, status } = req.body;

    // Validate user input
    if (!friendId && !status) {
      return res.status(400).send("All input is required");
    }

    if (!Object.values(STATUS_ENUM).includes(status)) {
      return res.status(400).send("Invalid status");
    }

    // get DB
    const db = getDB();

    // performing upsert
    let query = {
      $or: [
        {
          requestorId: ObjectId(req.user.user_id),
          accecptorId: ObjectId(friendId),
        },
        {
          requestorId: ObjectId(friendId),
          accecptorId: ObjectId(req.user.user_id),
        },
      ],
    };

    let updateQuery = {};

    if (status === STATUS_ENUM.REQUEST) {
      updateQuery.requestorId = ObjectId(req.user.user_id);
      updateQuery.accecptorId = ObjectId(friendId);
    } else if (status === STATUS_ENUM.ACCECPT) {
      updateQuery.requestorId = ObjectId(friendId);
      updateQuery.accecptorId = ObjectId(req.user.user_id);
    }

    const update = { $set: { ...updateQuery, status } };
    const options = { upsert: true };

    // user subscriptions
    await db
      .collection("user_subscriptions")
      .updateOne(query, update, options)
      .then((result) => {
        return res.status(201).json({ ...result });
      })
      .catch((e) => res.status(500).json({ e: JSON.stringify(e) }));
  } catch (e) {
    console.log(e);
    res.status(400).send(JSON.stringify(e));
  }
};

const getRequestingFriendsById = (userId) => {
  let query = [
    {
      $match: { requestorId: ObjectId(userId), status: STATUS_ENUM.REQUEST },
    },
    {
      $lookup: {
        from: "user_profiles",
        localField: "accecptorId",
        foreignField: "_id",
        as: "user_profile",
      },
    },
    {
      $unwind: "$user_profile",
    },
  ];
  return query;
};

const getFollowingFriendsById = (userId) => {
  let query = [
    {
      $match: { accecptorId: ObjectId(userId), status: STATUS_ENUM.REQUEST },
    },
    {
      $lookup: {
        from: "user_profiles",
        localField: "requestorId",
        foreignField: "_id",
        as: "user_profile",
      },
    },
    {
      $unwind: "$user_profile",
    },
  ];
  return query;
};

const getAllFriendsById = (userId) => {
  let query = [
    {
      $match: { requestorId: ObjectId(userId), status: STATUS_ENUM.ACCECPT },
    },
    {
      $project: {
        accecptorId: 1,
        friend_id: "$accecptorId",
      },
    },
    {
      $unionWith: {
        coll: "user_subscriptions",
        pipeline: [
          {
            $match: {
              accecptorId: ObjectId(userId),
              status: STATUS_ENUM.ACCECPT,
            },
          },
          {
            $project: {
              requestorId: 1,
              friend_id: "$requestorId",
            },
          },
        ],
      },
    },
    {
      $lookup: {
        from: "user_profiles",
        localField: "friend_id",
        foreignField: "_id",
        as: "friend_details",
      },
    },
    {
      $unwind: "$friend_details",
    },
  ];
  return query;
};

// to get friends based on its type FRIEND_TYPE_ENUM
const getFriendsByType = async (req, res) => {
  try {
    const { type } = req.body;

    // Validate type
    if (!type) {
      return res.status(400).send("type is required");
    }

    if (!Object.values(FRIEND_TYPE_ENUM).includes(type)) {
      return res.status(400).send("Invalid type");
    }

    // get DB
    const db = getDB();

    let query;

    switch (type) {
      case FRIEND_TYPE_ENUM.REQUESTING:
        query = getRequestingFriendsById(req.user.user_id);
        break;
      case FRIEND_TYPE_ENUM.FOLLOWING:
        query = getFollowingFriendsById(req.user.user_id);
        break;
      case FRIEND_TYPE_ENUM.ALL:
        query = getAllFriendsById(req.user.user_id);
      default:
        break;
    }

    // user subscriptions
    await db
      .collection("user_subscriptions")
      .aggregate(query)
      .toArray()
      .then((list) => {
        return res.status(201).json({ list });
      })
      .catch((e) => res.status(500).json({ e: JSON.stringify(e) }));
  } catch (e) {
    console.log(e);
    res.status(400).send(JSON.stringify(e));
  }
};

module.exports = {
  getFriendsByType,
  friendSubscription,
};
