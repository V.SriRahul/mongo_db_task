const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
  const token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }

  try {
    let getToken = token.split(" ")?.[1];
    const decoded = jwt.verify(getToken, process.env.TOKEN_KEY);
    req.user = decoded;
  } catch (err) {
    return res.status(401).send(JSON.stringify(err));
  }
  return next();
};

module.exports = verifyToken;
